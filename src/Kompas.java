public class Kompas {

    Kompas (Double otklonIgle){
        this.otklonIgle = otklonIgle;
    }

    public void setOtklonIgle(Double otklonIgle) {
        this.otklonIgle = otklonIgle;
    }

    public Double getOtklonIgle() {
        return otklonIgle;
    }


    void pokaziSmjer(){
        if (otklonIgle == 0){
            System.out.println(Smjerovi.sjever.kratica);

        }else if(otklonIgle > 0 && otklonIgle < 90){
            System.out.println(Smjerovi.sjeveroistok.kratica);

        }else if(otklonIgle == 90){
            System.out.println(Smjerovi.istok.kratica);

        }else if(otklonIgle > 90 && otklonIgle < 180){
            System.out.println(Smjerovi.jugoistok.kratica);

        }else if(otklonIgle == 180){
            System.out.println(Smjerovi.jug.kratica);

        }else if(otklonIgle > 180 && otklonIgle < 270){
            System.out.println(Smjerovi.jugozapad.kratica);

        }else if(otklonIgle == 270){
            System.out.println(Smjerovi.zapad.kratica);

        }else if(otklonIgle > 270 && otklonIgle < 315){
            System.out.println(Smjerovi.sjeverozapad.kratica);

        }
    }

    Double otklonIgle;
}
