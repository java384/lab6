public class Macka implements Printable {

    Macka (String ime,int godine){
        this.ime = ime;
        this.godine = godine;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public void setGodine(int godine) {
        this.godine = godine;
    }

    public String getIme() {
        return ime;
    }

    public int getGodine() {
        return godine;
    }

    @Override
    public void print() {
        System.out.println("mjau mjau");

    }

    String ime;
    int godine;
}
