public enum Smjerovi {

    sjever ("S"),
    jug ("J"),
    istok ("I"),
    zapad ("Z"),
    sjeveroistok ("SI"),
    sjeverozapad ("SZ"),
    jugozapad ("JZ"),
    jugoistok ("JI");


    String kratica;

    Smjerovi (String kratica){
        this.kratica = kratica;
    }

    public void setKratica(String kratica) {
        this.kratica = kratica;
    }

    public String getKratica() {
        return kratica;
    }
}
